package com.expressbank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootGraphqlRestApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootGraphqlRestApiApplication.class, args);
    }

}
